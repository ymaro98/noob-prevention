import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Abonnementen from '../views/Abonnementen.vue'
import BoxDetail from '../views/BoxDetail.vue'
import OnsTeam from '../views/OnsTeam.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/abonnementen',
    name: 'Abonnementen',
    component: Abonnementen
  },
  {
    path: '/abonnementen/:boxId',
    name: 'BoxDetail',
    component: BoxDetail
  },
  {
    path: '/ons-team',
    name: 'OnsTeam',
    component: OnsTeam
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
