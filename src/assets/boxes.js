export const boxes = [
    {
        "id": 1,
        "name": 'SOLDIER', 
        "img": {
            "src": 'images/boxes/soldier/icon.svg',
            "alt": 'Soldier icon'
        }, 
        "intro": 'Ben jij nog een noob? Geeft helemaal niet. Met deze box leggen we alle basis elementen van de game uit.',
        "full": 'Ben jij nog een noob? Geeft helemaal niet. Met dit abonnement leggen we alle basis elementen van de game uit in de standaard tip video’s. Daarbij kunnen wij een gameplay van je analyseren en je een uur lang begeleiden in game',
        "details":[
            {"id": 1, "txt": 'Toegang tot de 10 standaard tip video’s'},
            {"id": 2, "txt": '1 gameplay analyse'},
            {"id": 3, "txt": '1x per week een uur live chat'},
        ],
        "price": '€39,95' 
    },
    {
        "id": 2,
        "name": 'SERGEANT', 
        "img": {
            "src": 'images/boxes/sergeant/icon.svg',
            "alt": 'Seargeant icon'
        }, 
        "intro": 'Je hebt de basics down, maar je K/D is nog net niet boven de 1? Dan is dit het perfecte abonnement voor jou!',
        "full": 'Je hebt de basics down, maar je K/D is nog net niet boven de 1? Dan is dit het perfecte abonnement voor jou. Je kan altijd de basic terug kijken, maar we hebben ook nog een paar extra shooting tips voor je. Daarnaast kunnen we 3 gameplays analyseren, zodat we de progressie van voor, tijdens en na de tips kunnen bekijken en vergelijken',
        "details":[
            {"id": 1, "txt": 'Toegang tot de 10 standaard tip video’s'},
            {"id": 2, "txt": 'Toegang tot de 2 In-Depth premium tip video’s'},
            {"id": 3, "txt": '3 gameplay analyse'},
            {"id": 4, "txt": '2x per week een uur live chat'},
        ],
        "price": '€64,99' 
    },
    {
        "id": 3,
        "name": 'CAPTAIN', 
        "img": {
            "src": 'images/boxes/captain/icon.svg',
            "alt": 'Captain icon'
        }, 
        "intro": 'Je K/D is al redelijk hoog, maar je mist nog net de "details"? Dit abonnement kan je van een casual naar een pro maken.',
        "full": 'Je K/D is al redelijk hoog, maar je mist nog net de "details"? Dit abonnement kan je van een casual naar een pro maken. In dit abonnement kunnen wij, door middel van de 5 gameplays analyses echt goed bekijken welke bad habits jij hebt, zodat wij hier van af kunnen komen. Daarnaast gaan we ook in detail over het spawnsysteem, de HUD, bulletdrop en nog veel meer!',
        "details":[
            {"id": 1, "txt": 'Toegang tot de 10 standaard tip video’s'},
            {"id": 2, "txt": 'Toegang tot de 5 In-Depth premium tip video’s'},
            {"id": 3, "txt": '5 gameplay analyse'},
            {"id": 4, "txt": '4x per week een uur live chat'},
        ],
        "price": '€94,99' 
    },
]