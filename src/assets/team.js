export const team = [
    {
        "id": 1,
        "name": 'Carla Lucifer',
        "img": {
            "src": 'images/team/carla.svg',
            "alt": 'Afbeelding Carla'
        }, 
        "details":[
            {"id": 1, "txt": '1x wereld kampioenschap winnares'},
            {"id": 2, "txt": 'Nederlandse kampioenschap winnares'},
            {"id": 3, "txt": '2x wereld kampioenschap finaliste'},
            {"id": 4, "txt": 'Gekozen als een van de beste NL CoD spelers'},
        ],
        "show_details": false,
        "quote": "Don't limit your challenges, challenge your limits."
    },
    {
        "id": 2,
        "name": 'Cole Brent',
        "img": {
            "src": 'images/team/cole.svg',
            "alt": 'Afbeelding Cole'
        }, 
        "details":[
            {"id": 1, "txt": '3x wereld kampioenschap winnaar'},
            {"id": 2, "txt": '1x Nederlandse kampioenschap winnaar'},
            {"id": 3, "txt": '1x wereld kampioenschap finalist'},
            {"id": 4, "txt": 'Gekozen als een van de beste NL CoD spelers'},
        ],
        "show_details": false,
        "quote": "Hard work beats talent when talent doesn't work hard."
    },
]